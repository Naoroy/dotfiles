vim.o.background = 'dark'
vim.cmd.colorscheme('gruvbox')

vim.g.gruvbox_contrast_light='hard'
vim.g.gruvbox_contrast_dark='soft'
vim.g.ayucolor="light"

-- compatibility mode for zenbones theme (to replace lush)
vim.g.zenbones_compat = 1
vim.g.zenburned_compat = 1
vim.g.zenwritten_compat = 1

vim.keymap.set('', '<leader>bb', ':let &background = ( &background == "dark"? "light" : "dark" )<CR>')
