vim.g.lightline = {
  colorscheme= 'wombat',
  active = {
    left = {
      { 'mode', 'paste' },
      { 'gitbranch', 'relativepath', 'modified', 'readonly' }
    },
    right = {
      { 'lineinfo' },
      { 'filetype', 'fileformat' },
    }
  },
  inactive = {
    left = {
      { 'paste' },
      { 'gitbranch', 'relativepath', 'modified', 'readonly',  }
    },
    right = {
      { 'filetype', 'fileformat' },
    }
  },
  component_function = {
    gitbranch = 'FugitiveHead'
  },
}
