-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use({ 'wbthomason/packer.nvim' })
  use({
    'nvim-telescope/telescope.nvim',
    tag = '0.1.x',
    requires = { {'nvim-lua/plenary.nvim'} }
  })
  use({ 'rose-pine/neovim',
    as = 'rose-pine',
    config = function() vim.cmd('colorscheme rose-pine') end
  })
  use({ 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' })
  use({ 'tpope/vim-fugitive' })
  use({
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v1.x',
    requires = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},
      {'williamboman/mason.nvim'},
      {'williamboman/mason-lspconfig.nvim'},

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},
      {'hrsh7th/cmp-buffer'},
      {'hrsh7th/cmp-path'},
      {'saadparwaiz1/cmp_luasnip'},
      {'hrsh7th/cmp-nvim-lsp'},
      {'hrsh7th/cmp-nvim-lua'},

      -- Snippets
      {'L3MON4D3/LuaSnip'},
      {'rafamadriz/friendly-snippets'},
    }
  })
  use({ 'psliwka/vim-smoothie' })
  use({ 'tpope/vim-commentary' })
  use({ 'airblade/vim-gitgutter' })
  use({ 'Yggdroot/indentLine' })
  use({ 'szw/vim-maximizer' })
  use({ 'mhinz/vim-startify' })
  use({ 'itchyny/lightline.vim' })
  use({ 'github/copilot.vim' })
  use({ 'junegunn/fzf.vim' })
  use({ 'ibhagwan/fzf-lua',
    -- optional for icon support
    requires = { 'nvim-tree/nvim-web-devicons' }
  })
  use({ 'djoshea/vim-autoread' })

  -- Debugger
  use({
    'mfussenegger/nvim-dap',
    module = { 'dap' },
    requires = {
      'theHamsta/nvim-dap-virtual-text',
      'rcarriga/nvim-dap-ui',
      'mfussenegger/nvim-dap-python',
      'nvim-telescope/telescope-dap.nvim',
      { 'leoluz/nvim-dap-go', module = 'dap-go' },
      { 'jbyuki/one-small-step-for-vimkind', module = 'osv' },
      { 'mxsdev/nvim-dap-vscode-js', module = { 'dap-vscode-js' } },
      {
        'microsoft/vscode-js-debug',
        run = 'npm install && npx gulp vsDebugServerBundle && mv dist out'
      },
    },
    config = function()
      require('dap').setup()
    end,
    disable = false,
  })
end)
