# path
set DENO    $HOME/.deno/bin
set GOPATH  $HOME/go/src
set NPM     $HOME/.npm/bin
set NVM     $HOME/.nvm/bin
set LUA     $HOME/.luarocks/bin
set CABAL   $HOME/.cabal/bin
set GHCUP   $HOME/.ghcup/bin
set LOCAL   $HOME/.local/bin

set -U fish_user_paths \
  $HOME/bin $DENO $NVM $NPM $LUA $CABAL $GHCUP $LOCAL

# aliases
#set wifi_device wlp0s12f0
#set wpa_conf /etc/wpa_supplicant/wpa_supplicant-wlp0s12f0.conf
#set rk61_mac DC:2C:26:C5:EB:78
set symbols /usr/share/X11/xkb/symbols
set gl https://gitlab.com
set gh https://github.com
set fr_bepo -layout "fr,fr" -variant "rk,bepo" -option "grp:menu_toggle"

