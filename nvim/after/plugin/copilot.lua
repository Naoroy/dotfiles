-- vim.keymap.set('i', "<silent><script><expr><C-a> ",vim.fn["copilot#Accept"])
vim.g.copilot_no_tab_map = true

vim.g.copilot_filetypes = {
  all = false,
  javascript = true,
}

vim.cmd("imap <silent><script><expr> <C-N> copilot#Next()")
vim.cmd("imap <silent><script><expr> <C-P> copilot#Previous()")
vim.cmd("imap <silent><script><expr> <C-J> copilot#Accept('<CR>')")

