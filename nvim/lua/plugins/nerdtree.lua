local keyset = vim.keymap.set

keyset('n', '<leader>pv', ':NERDTreeToggle<CR>')
