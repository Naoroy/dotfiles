local fzf = require('fzf-lua')

fzf.setup({})

vim.keymap.set('n', '<leader>q', function()
  fzf.grep_project()
end)

