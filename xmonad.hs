import XMonad
import XMonad.Config.Azerty
import qualified XMonad.StackSet as W

import XMonad.Util.EZConfig
import XMonad.Util.Ungrab
import XMonad.Util.Loggers

import XMonad.Layout.ThreeColumns
import XMonad.Layout.Magnifier
import XMonad.Layout.Renamed
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Layout.TwoPane
import XMonad.Layout.NoBorders

import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP


main :: IO()
main = xmonad
  . ewmhFullscreen
  . ewmh
  . withEasySB (statusBarProp "xmobar" (pure customXmobarPP)) defToggleStrutsKey
  $ customConfig

workspacesWithIcons =
    [ "<fn=1>\xf0c0</fn>" --socials
    , "<fn=1>\xf192</fn>" --web
    , "<fn=1>\xf121</fn>" --work
    , "<fn=1>\xf58f</fn>" --misc
    ]

customConfig = azertyConfig
    { terminal            = "kitty"
    , modMask             = mod4Mask
    , focusFollowsMouse    = True
    , borderWidth         = 2
    , workspaces          = workspacesWithIcons
    , normalBorderColor   = "#777777"
    , focusedBorderColor  = "#ff5586"
    , layoutHook          = customLayout
    , startupHook         = customStartup
    }
  `additionalKeysP`
    -- File explorer
    [ ("M-e", spawn "thunar")
    -- Screenshot
    , ("M-S-=", unGrab *> spawn "flameshot full -c")
    -- close focused window
    , ("M-<Esc>", kill)
    -- launch new terminal
    , ("M-<Return>", spawn "kitty")
    -- promote focused window to master
    , ("M-C-<Return>", windows W.swapMaster)
    -- spawn dmenu
    , ("M-p", spawn "dmenu_run -b -i -l 20")
    ]
  `removeKeysP`
    [ "M-S-<Return>"
    , "M-S-c"
    ]

customStartup = do
  spawn trayer
  spawn "xsetroot -cursor_name left_ptr"
  spawn "set_lang"
  spawn "feh --bg-fill --no-fehbg ~/Images/wallpapers/42353.jpg"
  spawn "xbindkeys"
  spawn "bluetoothctl power on"
  where
    trayer = "killall trayer; trayer --edge top --align right --SetPartialStrut true --width 5 \
      \ --transparent true --tint 0x333333 --padding 10 --height 34 &"

customLayout =
  tiled
  ||| tabbed
  ||| column
  where
    column  = renamed [ Replace "col" ]
              $ magnifiercz' 1.4 $ ThreeColMid nmaster delta ratio
    tiled   = renamed [ Replace "til" ]
              $ smartSpacing 18 $ Tall nmaster delta ratio
    tabbed  = renamed [ Replace "tab" ]
              $ noBorders $ simpleTabbedBottom
    nmaster = 1       -- Default number of windows in the master pane
    ratio   = 1.7/3     -- Default proportion of screen occupied by master pane
    delta   = 0.05   -- Percent of screen to increment by when resizing panes

customXmobarPP :: PP
customXmobarPP = def
  { ppSep             = magenta " • "
  , ppTitleSanitize   = xmobarStrip
  , ppCurrent         = highBlue . wrap " " "" . xmobarBorder "Bottom" "#8be9fd" 5
  , ppHidden          = white . wrap " " ""
  , ppHiddenNoWindows = lowWhite . wrap " " ""
  , ppUrgent          = red . wrap (yellow "!") (yellow "!")
  , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
  , ppExtras          = [logTitles formatFocused formatUnfocused]
  }
  where
    formatFocused   = wrap (white     "[") (white   "]") . magenta . ppWindow
    formatUnfocused = wrap (white  "[") (white  "]") . lowWhite . ppWindow
    -- Windows should have *some* title, which should not not exceed a
    -- sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 20 

    --highBlue, blue, lowBlue, lowWhite, magenta, red, white, yellow :: String -> String

    magenta   = xmobarColor "#ff5586" ""
    blue      = xmobarColor "#7d70e2" ""
    white     = xmobarColor "#f8f8f2" ""
    yellow    = xmobarColor "#f1fa8c" ""
    red       = xmobarColor "#fe4400" ""
    lowBlue   = xmobarColor "#4d30d2" ""
    lowWhite  = xmobarColor "#777777" ""
    highBlue  = xmobarColor "#8be9fd" ""
    orange    = xmobarColor "#feee3a" ""
    green     = xmobarColor "#40fe10" ""

