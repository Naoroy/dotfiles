vim.api.nvim_create_user_command('BackgroundToggle', function ()
  if vim.o.background == "dark" then
    vim.o.background = "light"
  else
    vim.o.background = "dark"
  end
end, {})

vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Edit / Reload config
vim.keymap.set("n", "<leader>ep", ":e ~/dotfiles/nvim/lua<CR>") -- [E]dit [P]lugins files
vim.keymap.set("n", "<leader>ec", ":e ~/dotfiles/nvim/init.lua<CR>")  -- [E]dit [C]onfig
vim.keymap.set("n", "<leader>rc", ":source ~/dotfiles/nvim/init.lua<CR>") -- [R]eload [C]onfig

--Window shortcut
vim.keymap.set("n", "<leader>w", "<C-w>")
vim.keymap.set("n", "<leader>b", ":Telescope buffers<CR>")

-- Move around panes
vim.keymap.set("n", "<leader>j", "<C-w>j")
vim.keymap.set("n", "<leader>k", "<C-w>k")
vim.keymap.set("n", "<leader>h", "<C-w>h")
vim.keymap.set("n", "<leader>l", "<C-w>l")

-- Move around code
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

-- Remove highlighting search results
-- vim.keymap.set("n", "<esc><esc>", ":silent! nohls<cr>")

-- Sane terminal access
vim.keymap.set("n", "<leader>t", ":bel sp | resize 10 | term<cr>")
vim.keymap.set("t", "<esc>", "<C-\\><C-n>")

-- Keep cursor centered
vim.keymap.set("n", "<C-d>", vim.fn['smoothie#downwards'])
vim.keymap.set("n", "<C-u>", vim.fn['smoothie#upwards'])
vim.keymap.set("n", "n", "nzz")
vim.keymap.set("n", "N", "Nzz")

-- Greatest remap ever
vim.keymap.set("x", "<leader>p", "\"_dP")
-- Second greatest remap ever
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- Copy file name to clipboard
vim.api.nvim_create_user_command('CopyFileName', function ()
  vim.cmd [[let @+ = expand('%')]]
end, {})

return M
