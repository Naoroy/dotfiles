### Exports ###
export ZSH="$HOME/.oh-my-zsh"
export PATH="/usr/local/opt/mongodb-community@5.0/bin:$HOME/.local/bin/:$HOME/bin:$PATH"
export GEOIP_DB_PATH="/Users/fegan/dev/o5r/config/GeoIP_DB"
export HOMEBREW_PREFIX="/usr/local"
export FZF_DEFAULT_COMMAND='rg --files --follow --no-ignore-vcs --hidden -g "!{node_modules/*,.git/*,dist/*,documentation/*}"'

### Theme ###
ZSH_THEME="wezm"
ZSH_THEME_RANDOM_CANDIDATES=(
  "alanpeabody"
  "kafeitu"
  "daveverwer"
  "mgutz"
  "muse"
  "aussiegeek"
  "alanpeabody"
  "macovsky-ruby"
  "agnoster"
  "takashiyoshida"
  "tonotdo"
  "af-magic"
  "gallois"
  "philips"
  "wezm"
)

### Plugins ###
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
plugins=(git themes emoji)
source $ZSH/oh-my-zsh.sh

### Aliases ###
alias save_theme="echo $RANDOM_THEME >> ~/Documents/zsh_themes"
alias open_kue="open http://127.0.0.1:3050 & ~/dev/projects/api.october.eu/node_modules/kue/bin/kue-dashboard -p 3050 -r redis://127.0.0.1:6379"
alias clear_kue="NODE_PATH=~/dev/projects/api.october.eu/dist node ~/dev/projects/api.october.eu/dist/scripts/cleanKue.js"
alias sd="cd ~ && cd \$(rg --hidden --files --null -g '!{Library/*,node_modules/*,.git/*,dist/*,documentation/*}' | xargs -0 dirname | uniq | fzf)"


### Functions ###
#Emoji shorhands
# Usage: mj [emoji name or shorthand]
function mj() { echo $emoji[$1] }

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# Source ENV for LW
# source $HOME/.env.zsh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

[ -f "/Users/fegan/.ghcup/env" ] && source "/Users/fegan/.ghcup/env" # ghcup-env

# Keep NVM up to date
autoload -U add-zsh-hook
load-nvmrc() {
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$(nvm version)" ]; then
      nvm use
    fi
  elif [ -n "$(PWD=$OLDPWD nvm_find_nvmrc)" ] && [ "$(nvm version)" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
