local keyset = vim.keymap.set
--[[ Keybindings ]]--
-- Leader key
vim.g.mapleader = ' '

-- Edit / Reload config
keyset('n', '<leader>ep', ':e ~/dotfiles/nvim/lua<CR>') -- [E]dit [P]lugins files
keyset('n', '<leader>ec', ':e ~/dotfiles/nvim/init.lua<CR>')  -- [E]dit [C]onfig
keyset('n', '<leader>rc', ':source ~/dotfiles/nvim/init.lua<CR>') -- [R]eload [C]onfig

-- Disable arrow keys
keyset('', '<up>', '<nop>')
keyset('', '<down>', '<nop>')
keyset('', '<left>', '<nop>')
keyset('', '<right>', '<nop>')

--Window shortcut
keyset('n', '<leader>w', '<C-w>')

-- Move around panes
keyset('n', '<leader>j', '<C-w>j')
keyset('n', '<leader>k', '<C-w>k')
keyset('n', '<leader>h', '<C-w>h')
keyset('n', '<leader>l', '<C-w>l')

-- Move around code
keyset('v', 'J', ':m \'>+1<CR>gv=gv')
keyset('v', 'K', ':m \'<-2<CR>gv=gv')
keyset('v', '<', '<gv')
keyset('v', '>', '>gv')

-- Remove highlighting search results
keyset('n', '<esc><esc>', ':silent! nohls<cr>')

-- Sane terminal access
keyset('n', '<leader>t', ':sp | resize 10 | term<cr>')
keyset('t', '<esc>', '<C-\\><C-n>')

-- find/replace
keyset('n', '<C-f>', ':%s/')
