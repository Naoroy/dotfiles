function ls
    command ls -x1 --group-directories-first --color $argv
end