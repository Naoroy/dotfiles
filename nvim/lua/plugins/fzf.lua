local keyset = vim.keymap.set

keyset('n', '<leader>f', ':FZF<CR>')
keyset('n', '<leader>q', ':Rg<CR>')
keyset('n', '<leader>gc', ':GCheckout<CR>')

vim.g.fzf_layout = {
  window = {
    width = 0.8,
    height = 0.8,
    yoffset = 0.5,
  }
}
