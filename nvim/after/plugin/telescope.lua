local telescope = require('telescope')
local builtin = require('telescope.builtin')

telescope.setup({
  defaults = {
    file_ignore_patterns = {
      "node_modules", "build", "dist", ".git"
    },
  }
})

vim.keymap.set('n', '<leader>f', function()
  builtin.find_files()
end, {})

-- vim.keymap.set('n', '<leader>q', function()
--   builtin.live_grep()
-- end, {})
