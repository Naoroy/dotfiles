local keyset = vim.keymap.set

keyset('n', '<leader>m', ':MaximizerToggle!<CR>')
