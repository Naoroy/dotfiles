local lsp = require("lsp-zero")
local cmp = require('cmp')
local lspconfig = require("lspconfig")

local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
  ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
  ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
  ['<C-y>'] = cmp.mapping.confirm({ select = true }),
  ["<C-Space>"] = cmp.mapping.complete(),
})
lsp.preset("recommended")
lsp.ensure_installed({
  'tsserver',
  'hls',
  'eslint',
})
lsp.setup_nvim_cmp({
  mapping = cmp_mappings
})
lsp.on_attach(function(_, bufnr)
  local opts = { buffer = bufnr, remap = false }

  vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
  vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
  vim.keymap.set("n", "<leader>gws", function() vim.lsp.buf.workspace_symbol() end, opts)
  vim.keymap.set("n", "<leader>gd", function() vim.diagnostic.open_float() end, opts)
  vim.keymap.set("n", "<leader>gn", function() vim.diagnostic.goto_next() end, opts)
  vim.keymap.set("n", "<leader>gp", function() vim.diagnostic.goto_prev() end, opts)
  vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
  vim.keymap.set("n", "<leader>gr", function() vim.lsp.buf.references() end, opts)
  vim.keymap.set("n", "<leader>rn", function() vim.lsp.buf.rename() end, opts)
  vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)

  -- Show errors on cursor hold
  -- vim.api.nvim_create_autocmd("CursorHold", {
  --   buffer = bufnr,
  --   callback = function()
  --     vim.diagnostic.open_float(nil, {
  --       focusable = false,
  --       close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
  --       source = 'always',
  --       prefix = ' ',
  --       scope = 'cursor',
  --     })
  --   end
  -- })
  lspconfig.lua_ls.setup({
    settings = {
      Lua = {
        diagnostics = {
          globals = { 'vim' }
        }
      }
    },
    flags = {
        allow_incremental_sync = false,
        -- debounce_text_changes = <some_integer_value>  -- In ms
    }
  })

  vim.diagnostic.config({
    virtual_text = true
  })
end)

lsp.setup()
